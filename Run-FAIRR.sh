#!/bin/sh

echo "Step-1:--------- add dcmqi and dcm2niix to path-------"
export PATH=$PATH:~/anaconda3/bin
export PATH=$PATH:/home/ubuntu/FAIR-Radiomics/dcmqi/bin
export PATH=$PATH:/home/ubuntu/FAIR-Radiomics/dcm2niix
echo "Step-2:--------- Running FAIR-Radiomics main script-------"
python FAIR-Radiomics.py