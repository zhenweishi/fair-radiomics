# FAIR-Radiomics
## Findable(F), Accessible(A), Interoperability(I) and Reuse (R) - Radiomics

FAIR-Radiomics tool is able to calculate radiomic feature complying with FAIR principles:  (i) radiomics data and extraction details could be published with a Findable(F) and unique identifier; (ii) radiomics data and metadata are described with radiomics ontology, which make them accessible(A) and understandable by machines and humans; (iii) data uses a formal, standardized and applicable ontology for knowledge representation, which makes interoperability(I) among multi-centres possible; (iv) data offers explicit information on provenance and licenses for reuse (R).

## Citation
The publication of FAIR-Radiomics is coming soon. Please cite the webpage when you use it for academic research.

## *** Warning

The current version of FAIR-Radiomics is developed on Ubuntu 18.04. The program should be compatible for Ubuntu 16.04 or higher. For other Linux OS, only the packages installation way is different. For Windows and Mac OS, the developers have not test yet, but will support in future.

## Disclaimer

FAIR-Radiomics is still under development. Although we have tested and evaluated the workflow under many different situations, errors and bugs still happen unfortunately. Please use it cautiously. If you find any, please contact us and we would fix them ASAP.


### Prerequisites 

FAIR-Radiomicsis dependent on several tools and packages that are listed below.

1. [Anaconda](https://www.anaconda.com/download/) python 3 version, which includes python and hundreds of popular data science packages and the conda package and virtual environment manager for Windows, Linux, and MacOS.
2. [Pyradiomics](https://github.com/Radiomics/pyradiomics) - radiomic extractor.
3. [plastimatch](https://www.plastimatch.org/) - convert images to nrrd files. 
4. [dcmqi](https://github.com/QIICR/dcmqi) - create DICOM Segmentation and DICOM Structured Reporting
5. [dcm2niix](https://github.com/rordenlab/dcm2niix) - DICOM format to the NIfTI format


### Installation

The bash script is able to finish the installation 

Execute:
```
bash FAIRR_initializer.sh
```
### Getting Started

Execute:
```
bash Run-FAIRR.sh
```

## License

FAIR-Radiomics may not be used for commercial purposes. This package is freely available to browse, download, and use for scientific and educational purposes as outlined in the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).

## Developers
 - [Zhenwei Shi](https://github.com/zhenweishi)<sup>1</sup>
 - [Leonard Wee]<sup>1</sup>
 - [Andre Dekker]<sup>1</sup>
 
<sup>1</sup>Department of Radiation Oncology (MAASTRO Clinic), GROW-School for Oncology and Development Biology, Maastricht University Medical Centre, The Netherlands.
