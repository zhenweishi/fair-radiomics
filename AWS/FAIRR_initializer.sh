#!/bin/sh

echo "Step-1:"
echo "-------- initial update-------"
sudo apt-get update

echo "Step-2:"
echo "-------- install Anaconda3-----" 
cd /tmp
wget https://repo.anaconda.com/archive/Anaconda3-5.2.0-Linux-x86_64.sh
bash Anaconda3-5.2.0-Linux-x86_64.sh
sudo rm Anaconda3-5.2.0-Linux-x86_64.sh
export PATH=$PATH:~/anaconda3/bin 

echo "Step-3:"
echo "-------- install pip and gcc ---------"
# pip has been install in anaconda, so
pip install msgpack
pip install --upgrade pip
sudo apt install gcc #yes

echo "Step-4:"
echo "--------- install latest pyradiomics-------"
cd ~
git clone https://github.com/Radiomics/pyradiomics.git
cd ~/pyradiomics
python -m pip install -r requirements.txt
python setup.py install

echo "Step-5:"
echo "--------- install FAIR-Radiomics package-------"
cd ~
#git clone https://github.com/zhenweishi/FAIR-Radiomics.git
#cd ./FAIR-Radiomics
python -m pip install -r FAIRR_requirements.txt

echo "Step-6:--------- install plastimatch package-------"
sudo apt-get install plastimatch #Y
cd ~

echo "Step-7:--------- change exe files mode-------"
cd ~/FAIR-Radiomics/dcmqi/bin
sudo chmod u+x itkimage2segimage
sudo chmod u+x segimage2itkimage
sudo chmod u+x tid1500writer
cd ~/FAIR-Radiomics/dcm2niix
sudo chmod u+x dcm2niibatch
sudo chmod u+x dcm2niix
cd ~

echo "Step-8:--------- install docker-------"
sudo apt install docker.io

# echo "Step 9:--------- download data from XNAT-------"
# cd ./FAIR-Radiomics
# python FAIR_Radiomics_XNAT.py


